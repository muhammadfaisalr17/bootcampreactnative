    //Start No 1
    console.log('Start No 1')
    var name = 'Junaedi'
    var peran = 'Werewolf'

    if (name === ''){
	    console.log("Nama harus diisi!")
    }else if (name === 'John'){
	    console.log("Halo John, Pilih peranmu untuk memulai game!")
    }
 
    if (peran === ''){
	    console.log('Halo John, Silahkan Pilih Peran Kamu : Werewolf | Guard | Penyihir')
    }else{
	    if(peran === 'Penyihir' && name === 'Jane'){
  	        console.log("Selamat datang di Dunia Werewolf, Jane\nHalo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
        }else if(peran === 'Guard' && name === 'Jenita'){
  	        console.log("Selamat datang di Dunia Werewolf, Jenita\nHalo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
        }else if(name === 'Junaedi' && peran === 'Werewolf'){
  	        console.log("Selamat datang di Dunia Werewolf, Junaedi\nHalo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!" )
        }
    }
    console.log('End No 1\n\n')
    //End No 1

    //Start No 2
    console.log('Start No 2')
    var tanggal = 17;
    var bulan = 11; 
    var tahun = 2000;

    var bulanString = "";

    switch(bulan) {
        case 1:   { bulanString = 'Januari'; break; }
        case 2:   { bulanString = 'Februari'; break; }
        case 3:   { bulanString = 'Maret'; break; }
        case 4:   { bulanString = 'April'; break; }
 	    case 5:   { bulanString = 'Mei'; break; }
	    case 6:   { bulanString = 'Juni'; break; }
	    case 7:   { bulanString = 'Juli'; break; }
	    case 8:   { bulanString = 'Agustus'; break; }
	    case 9:   { bulanString = 'September'; break; }
        case 10:   { bulanString = 'Oktober'; break; }
        case 11:   { bulanString = 'November'; break; }
    default:  { bulanString = 'Desember'; }}

    console.log(tanggal + " " + bulanString + " " + tahun)
    console.log('End No 2')
    //End No 2