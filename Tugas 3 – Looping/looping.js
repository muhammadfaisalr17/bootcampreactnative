  //Start No 1
  console.log('Start No 1')
    var position = 1;
    var text = 'I Love Coding'
    
    console.log('Looping Pertama'.toUpperCase())
    while(position <= 20){
      position++
        if(position % 2 === 0){
        console.log(position + ' - ' + text);
      }
    }
    
    console.log('Looping Kedua'.toUpperCase())
    while(position >= 1){
        text = 'I will become a mobile developer';
        position--
        if(position % 2 === 0){
        console.log(position + ' - ' + text);
      }
    }
    console.log('End No 1\n\n')
  //End No 1

  //Start No 2
  console.log("Start No 2")
    for(var i = 1; i <= 20; i++){
        if (i % 2 === 0){
          console.log(i + " - Berkualitas")
        }else if(i % 3 === 0){
          console.log(i + " - I Love Coding")
        }else{
          console.log(i + " - Santai")
        }
      }  
  console.log('End No 2\n\n')  
  //End No 2

  //Start No 3
  console.log('Start No 3')
    var hashtag = '########'

    for(var i = 0; i < 4; i ++){
      console.log(hashtag)
    }
  console.log('End No 3\n\n')
  //End No 3

  //Start No 4
  console.log('Start No 4')
    var hashtag = ''

    for(var i = 0; i < 7; i++){
      console.log(hashtag+="#")
    }
  console.log('End No 4\n\n')
  //End No 4

  //Start No 5
  console.log('Start No 5')
    var white = ' # # # #'
    var black = '# # # # '
    
    for(var i = 0; i < 8; i++){
      if(i % 2 === 0){
        console.log(white)
      }else{
        console.log(black)
      }
    }
    console.log('End No 5')
    //End No 5
