// Start No 1

console.log('Start No 1')

function teriak(){
    return 'Halo Sanbers!'
}

console.log(teriak())

console.log('End No 1\n\n')

//End No 1


//Start No 2

console.log('Start No 2')

var num1 = 12
var num2 = 4

function kalikan(a, b){
    return a * b
}

var hasilKali = kalikan(num1, num2)
console.log(hasilKali)

console.log('End No 2\n\n')

//End No 2


//Start No 3
console.log('Start No 3')
var name = 'Muhammad Faisal R'
var age = '20'
var address = 'Jakarta, Indonesia'
var hobby = 'Riding'

function introduce(nama, umur, alamat, hobi){
    return 'Nama saya '+nama+', umur saya '+umur+' tahun, alamat saya di '+alamat+', dan saya punya hobby yaitu '+hobi+'!'
}

var perkenalkan = introduce(name, age, address, hobby)

console.log(perkenalkan)

console.log('End No 3\n\n')


//End No 3