
//Start No 1
console.log("Start No 1")

function range(numA, numB){

    var number = []
  
    if (numA < numB){
      for (var i = numA; i <= numB; i++) {
         number.push(i)
      }
      return number
    }else if(numA > numB){
      for(var i = numA; i >= numB; i--){
        number.push(i)
      }
  
      return number
    }else{
      return -1
    }
  }
  
  
  console.log(range(1, 10))
  console.log(range(1)) 
  console.log(range(11,18))
  console.log(range(54, 50))
  console.log(range()) 

console.log("End No 1");
//End No 1

//Start No 2
console.log('Start No 2');

function rangeWithStep(numA, numB, step){

  var number = []

  if (numA < numB){
    for (var i = numA; i <= numB; i += step) {
       number.push(i)
    }
    return number
  }else if(numA > numB){
    for(var i = numA; i >= numB; i -= step){
      number.push(i)
    }

    return number
  }else{
    return -1
  }
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

console.log('End No 2');
//End No 2


//Start No 3 
console.log('Start No 3');

function sum(numA, numB, step){

  var number = []

  if (numA == null){
    return 0
  }
  
  if(step == null){
    if (numB == null){  
      return numA
    }else if(numA < numB){
      for (let i = numA; i <= numB; i++) {
        number.push(i)
      }
      return number.reduce((a, b) => a+b)

    }else{
      for (let i = numB; i <= numA; i++) {
        number.push(i)
      }
      return number.reduce((a, b) => a + b)
    }
    
  }else{
    if (numA < numB){
      for (let i = numA; i <= numB; i+= step) {
        number.push(i)
      }

      return number.reduce((a, b) => a+b)
    }else{
      for (let i = numB; i <= numA; i+= step) {
        number.push(i)
      }
      return number.reduce((a, b) => a + b)
    }
  }
}

console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())

console.log('End No 3');
//End No 3


//Start No 4
console.log('Start No 4');



var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

for (let i = 0; i < input.length; i++) {
    const item = input[i];
    console.log('Nomor ID : ' + item[0])
    console.log('Nama Lengkap :' + item[1])
    console.log('TTL :' + item[2] + ', ' + item[3])
    console.log('Hobi :' + item[4])
    console.log('\n')
}

console.log('End No 4');
//End No 4


//Start No 5
console.log('Start No 5');

function balikKata(data) {
  var string = "";
  for (let i = data.length - 1; i >= 0; i--) {
    string += data[i]
  }

  return string
}

console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar")) 
console.log(balikKata("I am Sanbers")) 

console.log('End No 5');
//End No 5


//Start No 6
console.log('Start No 6');

var input = [
  '0001',
  'Roman Alamsyah ',
  'Bandar Lampung',
  '21/05/1989',
  'Membaca',
];
dataHandling2(input);

function dataHandling2(data) {
  data.splice(1, 1, 'Roman Alamsyah Elsharawy');
  data.splice(2, 1, 'Provinsi Bandar Lampung');
  data.splice(4, 1, 'Pria');
  data.splice(5, 4, 'SMA Internatsional Metro');

  console.log(data);

  getMonth(data[3]);

  orderDateDesc(data[3])

  replaceDate(data[3])

  getName(data[1])
}

function getName(data){
  console.log(data.slice(0, 14))
}

function replaceDate(data){
  var datas = data.split('/')
  console.log(datas.join('-'))
}

function orderDateDesc(data){
  var datas = data.split('/')
  datas.sort(
    function(a, b){
      return b - a
    }
  )
  
 console.log(datas)
}

function getMonth(data) {
  var datas = data.split('/');

  var bulanString = datas[1][1];

  switch (bulanString) {
    case '1': {
      bulanString = 'Januari';
      break;
    }
    case '2': {
      bulanString = 'Februari';
      break;
    }
    case '3': {
      bulanString = 'Maret';
      break;
    }
    case '4': {
      bulanString = 'April';
      break;
    }
    case '5': {
      bulanString = 'Mei';
      break;
    }
    case '6': {
      bulanString = 'Juni';
      break;
    }
    case '7': {
      bulanString = 'Juli';
      break;
    }
    case '8': {
      bulanString = 'Agustus';
      break;
    }
    case '9': {
      bulanString = 'September';
      break;
    }
    case '10': {
      bulanString = 'Oktober';
      break;
    }
    case '11': {
      bulanString = 'November';
      break;
    }
    default: {
      bulanString = 'Desember';
    }
  }

  console.log(bulanString);
}

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */


console.log('End No 6');
//End No 6