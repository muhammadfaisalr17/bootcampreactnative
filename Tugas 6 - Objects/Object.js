
//Start No 1
console.log('Start No 1');
function arrayToObject(arr) {
    // Code di sini 
    if (arr.length == 0){
        return console.log('Tidak Ada Data Tersedia')
    }else{
        for (let i = 0; i < arr.length; i++) {
            const element = arr[i];
    
            var now = new Date()
            var thisYear = now.getFullYear()
    
            var fullName = element[0] + ' ' + element[1]
            var age;
    
            if(thisYear - element[3] < 0 || element[3] == null){
                age = 'Invalid Birth Day Year'
            }else{
                age = thisYear - element[3]
            }
    
            var data =  {
                'firstName' : element[0],
                'lastName' : element[1],
                'gender' : element[2],
                'age' :  age
            }
    
            console.log(fullName + ' = ' + JSON.stringify(data))
        
        } 
    }
   
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
 
// Error case 
arrayToObject([]) // ""
console.log('End No 1\n');
//End No 1

//Start No 2
console.log('Start No 2');

function shoppingTime(memberId, money) {
    // you can only write your code here!
    var listPurchased = []  

    if(memberId != null && money != null){
        
            var changeMoney;
            if(money < 50000){
                return 'Mohon Maaf, Uang Tidak Cukup'
            }else{
    
                if(money == 2475000){
                    listPurchased.push('Sepatu Scattu')
                    listPurchased.push('Baju Zoro')
                    listPurchased.push('Baju H&N')
                    listPurchased.push('Sweater Uniklooh')
                    listPurchased.push('Casing Handphone')
                    changeMoney = 0;
                }else  if (money == 170000){
                    listPurchased.push('Casing Handphone')
                    changeMoney = 120000
                }
    

                var data = {
                    'memberId' : memberId,
                    'money' : money,
                    'listPurchased' : listPurchased,
                    'changeMoney' : changeMoney
                }
            
                return JSON.stringify(data)
            }
    
        
    }

    return 'Mohon maaf, toko X hanya berlaku untuk member saja'

  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000))
  console.log(shoppingTime('82Ku8Ma742', 170000))
  console.log(shoppingTime('', 2475000)) //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)) //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log('End No 2');
//End No 2


//Start No 3 
console.log('Start No 3');

function naikAngkot(arr) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F']

    var finalOutput = []
    var data = {}

    if(arr.length <= 0){
        return []
    }

    var price = 0;

    for (let i = 0; i < arr.length; i++) {
    
        for (let j = 0; j < rute.length; j++) {
            if(arr[i][2] == rute[j]){
                var indexStart =  rute.findIndex(function(e1){
                    if(e1 == arr[i][1]){
                        return e1   
                    }
                })

                console.log(indexStart);

                price = (j - indexStart )* 2000
            }
            
        }

        finalOutput.push(
            data = {
                'penumpang' : arr[i][0],
                'naikDari' : arr[i][1],
                'tujuan' : arr[i][2],
                'harga' : price
            }
        )
    }

    return finalOutput

}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log([]);


console.log('End No 3');
//End No 3