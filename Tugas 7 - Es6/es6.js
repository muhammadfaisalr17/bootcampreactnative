{
    //Start No 1

    console.log('Start No 1');
    goldenFunction = () => {
        console.log('This is Golden');
    }

    goldenFunction()

    console.log('End No 1');
    //End No 1
}
{
    //Start No 2
    console.log('Start No 2');
    const newFunction = (firstName, lastName) => {
        let fullname = firstName + ' ' + lastName
        return arr = {
            firstName,
            lastName,
            fullname
        }
      }
       
    //Driver Code 
    console.log(newFunction('William', 'Imoh').fullname)
    console.log('End No 2');
    //End No 2
}
{
    //Start No 3
    console.log('Start No 3');

    const newObject = {
        firstName: "Harry",
        lastName: "Potter Holt",
        destination: "Hogwarts React Conf",
        occupation: "Deve-wizard Avocado",
        spell: "Vimulus Renderus!!!"
      }

    const {firstName , lastName, destination, occupation } = newObject
    
    console.log(firstName, lastName, destination, occupation)

    console.log('End No 3');
    //End No 3
}
{
    //Start No 4
    console.log('Start No 4');

    const west = ["Will", "Chris", "Sam", "Holly"]
    const east = ["Gill", "Brian", "Noel", "Maggie"]
    const combinedArray = [...west, ...east]
    
    //Driver Code
    console.log(combinedArray)

    console.log('End No 4');
    //End No 4
}
{
    //Start No 5
    console.log('Start No 5');

    const planet = "earth"
    const view = "glass"
    var before = `Lorem ${view} dolor sit amet, ` +  
    `consectetur adipiscing elit, ${planet} do eiusmod tempor ` +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim venia'
 
    // Driver Code
    console.log(before) 

    console.log('End No 5 ');
    //End No 5
}