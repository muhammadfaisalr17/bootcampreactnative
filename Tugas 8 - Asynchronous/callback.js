function readBooks(time, book, callback) {
    console.log(`saya membaca buku ${book.name}`);

    setTimeout(function () {
        let timeLeft = 0
        if(time > book.timeSpent){
            timeLeft = time - book.timeSpent
            console.log(`Saya sudah membaca buku ${book.name} dan Sisa Waktu Saya adalah ${timeLeft}`);
            callback(timeLeft)
        }else{
            console.log('Waktu saya sudah habis');
            callback(time)
        }
    }, book.timeSpent)
}

module.exports = readBooks