let readBooksPromise = require('./promise.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

function readBooks(){
    var time = 10000
    readBooksPromise(time, books[0])
        .then((data) => {
            readBooksPromise(data, books[1])
                .then((data) => {
                    readBooksPromise(data, books[2])
                    .then(() => {
                        return data
                    })
                    .catch((e) => {
                        console.log(e.message);
                    })
                })
                .catch((e) => {
                    console.log(e.message);
                })
        })
        .catch((e) => {
            console.log(e.message);
        })
}

readBooks()
