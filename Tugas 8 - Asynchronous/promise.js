function readBooksPromise(time, book) {
    console.log(`Saya mulai membaca buku ${book.name}`);

    return new Promise(function (resolve, reject) {
        setTimeout(() => {
            let timeLeft = time - book.timeSpent

            if(timeLeft >= 0){
                console.log(`Saya sudah selesai membaca buku ${book.name}, sisa waktu saya : ${timeLeft}`);
                resolve(timeLeft)
                return this
            }else{
                console.log(`Saya sudah tidak punya waktu untuk baca buku ${book.name}`);
                reject(timeLeft)
            }

        }, book.timeSpent);
    })
}

module.exports = readBooksPromise