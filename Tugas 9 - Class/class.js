{
    //Start No 1
    console.log('Start No 1');
    class Animal{
        constructor(name, legs, isColdBlood){
            this.name = name
            this.legs = legs
            this.isColdBlood = isColdBlood
        }
    }

    class Frog{
        constructor(name){
            this.name = name
        }

        jump() {
            return console.log('hop hop');
        }   
    }

    class Ape {
        constructor(name){
            this.name = name
        }

        yell(){
            return console.log('Auoo');
        }
    }

    let animal = new Animal("Shaun", 4, false)

    console.log(animal.name);
    console.log(animal.legs);
    console.log(animal.isColdBlood);

    let frog = new Frog('Buduk')
    frog.jump()

    let ape = new Ape('Kera Sakti')
    ape.yell()
}
{
    //Start No 2

    class Clock{
        constructor(template){
            this.template = template
        }

        render(){
            var date = new Date();

            var hours = date.getHours();
            if (hours < 10) hours = '0' + hours;
        
            var mins = date.getMinutes();
            if (mins < 10) mins = '0' + mins;
        
            var secs = date.getSeconds();
            if (secs < 10) secs = '0' + secs;
        
            var output = Object.values(this.template).toString()
              .replace('h', hours)
              .replace('m', mins)
              .replace('s', secs);
        
            console.log(output);
        }

        start(){
            this.render();
            this.timer = setInterval(() => this.render(), 1000);
        }

        stop(){
            clearInterval(this.timer)
        }
    }


    var clock = new Clock({template : 'h:m:s'})
    clock.start()
    //End No 2
}
